import xlrd
import xlsxwriter
from django.http import HttpResponseRedirect, HttpResponse, FileResponse
from django.shortcuts import render, reverse
from django.views import View
from test_app.forms import UploadExcelFileForm
from django.conf import settings
from test_app.models import ExcelFile
from django.utils.crypto import get_random_string
from django.contrib import messages
import copy

class UploadFileView(View):

    template_name = 'upload_file.html'

    def get(self,request):
        form = UploadExcelFileForm()
        file_data_objects = ExcelFile.objects.all()
        return render(request, self.template_name, {'form':form,'file_data_objects':file_data_objects})

    def post(self,request):
        form = UploadExcelFileForm(request.POST, request.FILES)
        if form.is_valid():
            excel_file = request.FILES['excel_file']
            file_name = excel_file.name
            self.handle_uploaded_file(excel_file)
            content_dict = self.convert_file_content_into_dict(file_name)
            row_count, col_count = self.get_row_and_col_count(content_dict)
            file_code_name = get_random_string(length=8)
            ExcelFile.objects.create(file_name=file_name,file_content=content_dict,file_code_name=file_code_name,row_count=row_count,col_count=col_count)
            return HttpResponseRedirect(reverse('show_as_table', args=(file_code_name,)))
        else:
            return render(request, self.template_name, {'form':form})

    def handle_uploaded_file(self,uploaded_file):
        with open(settings.MEDIA_ROOT + uploaded_file.name, 'wb+') as destination:
            for chunk in uploaded_file.chunks():
                destination.write(chunk)

    def get_row_and_col_count(self,content_dict):
        row_count = len(content_dict)
        col_count = 0
        for each_list in content_dict.values():
            col_count = len(each_list) if len(each_list) > col_count else col_count
        return row_count, col_count

    def convert_file_content_into_dict(self, file_name):
        content_dict = {}
        book = xlrd.open_workbook(settings.MEDIA_ROOT+file_name)
        for sheet in book.sheets():
            for r in range(sheet.nrows):
                content_dict.update({r:sheet.row_values(r)})
        return content_dict

class ShowAsTableView(View):

   template_name = 'show_as_table.html'

   def get(self, request, file_code_name):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        file_name = obj.file_name
        file_code_name = obj.file_code_name
        file_content = eval(obj.file_content)
        context_dict = {'file_content':file_content,'file_name':file_name,'file_code_name':file_code_name}
        return render(request, self.template_name, context_dict)


class UpdateFileView(View):

    template_name = 'update_file.html'

    def get(self,request,file_code_name):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        file_name = obj.file_name
        file_code_name = obj.file_code_name
        file_content = eval(obj.file_content)
        context_dict = {'file_content':file_content,'file_name':file_name,'file_code_name':file_code_name}
        return render(request, self.template_name, context_dict)

    def post(self,request,file_code_name):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        content_dict = self.format_data(request.POST,obj.row_count,obj.col_count)
        obj.file_content = content_dict
        obj.save()
        messages.success(request, 'changes saved')
        return HttpResponseRedirect(reverse('update_file', args=(file_code_name,)))

    def format_data(self,data,row_count,col_count):
        content_dict = {}
        for r in range(row_count-1):
            row_data = []
            for c in range(col_count-1):
                value = data['row_'+str(r)+'_col_'+str(c)]
                row_data.append(value)
            content_dict.update({r:row_data})
        return content_dict


class DownloadFile(View):

    def get(self,request,file_code_name):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        data = eval(obj.file_content)
        file_name = obj.file_name
        row_count = obj.row_count
        col_count = obj.col_count

        file_path = settings.MEDIA_ROOT + file_name
        workbook = xlsxwriter.Workbook(file_path)
        worksheet = workbook.add_worksheet()

        for r in range(row_count-1):
            row_data = data[r]
            for c in range(col_count-1):
                worksheet.write(r, c, row_data[c])

        workbook.close()
        file = open(file_path,'rb')
        return FileResponse(file,as_attachment=True)

class AddRowView(View):

    template_name = 'add_row.html'

    def get(self,request,file_code_name):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        return render(request, self.template_name,{'range':range(obj.col_count)})

    def post(self,request,file_code_name):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        data = copy.deepcopy(request.POST)
        data.pop('csrfmiddlewaretoken')
        row_count = obj.row_count + 1
        print(data)
        file_content = eval(obj.file_content)
        file_content.update({row_count:list(data.values())})
        obj.file_content = file_content
        obj.row_count = row_count
        obj.save()
        return HttpResponseRedirect(reverse('update_file',args=(file_code_name,)))

class DeleteRowView(View):

    def get(self,request,file_code_name,row_num):
        obj = ExcelFile.objects.get(file_code_name=file_code_name)
        file_content = eval(obj.file_content)
        del file_content[row_num]
        obj.file_content = file_content
        obj.row_count = obj.row_count - 1
        obj.save()
        return HttpResponseRedirect(reverse('update_file',args=(file_code_name,)))