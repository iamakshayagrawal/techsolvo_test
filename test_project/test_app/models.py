from django.db import models

class ExcelFile(models.Model):
    file_name = models.CharField(max_length=256)
    file_content = models.TextField()
    file_code_name = models.CharField(max_length=8)
    row_count = models.IntegerField(default=0)
    col_count = models.IntegerField(default=0)

    def __str__(self):
        return self.file_name