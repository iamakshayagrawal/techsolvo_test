from django import forms

class UploadExcelFileForm(forms.Form):
    excel_file = forms.FileField(widget=forms.ClearableFileInput(attrs={'accept': ".xlsx"}))

class AddUpdateRowForm(forms.Form):
    pass