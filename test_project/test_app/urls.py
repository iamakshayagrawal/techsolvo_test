from django.urls import path
from test_app.views import UploadFileView, ShowAsTableView, UpdateFileView, DownloadFile, AddRowView, DeleteRowView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', UploadFileView.as_view(), name='home'),
    path('show-as-table/<str:file_code_name>/', ShowAsTableView.as_view(), name='show_as_table'),
    path('update-file/<str:file_code_name>/', UpdateFileView.as_view(), name='update_file'),
    path('download-file/<str:file_code_name>/', DownloadFile.as_view(), name='download_file'),
    path('add-row/<str:file_code_name>/', AddRowView.as_view(), name='add_row'),
     path('delete-row/<str:file_code_name>/<int:row_num>/', DeleteRowView.as_view(), name='delete_row'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)